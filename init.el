(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "http://stable.melpa.org/packages/") t)

(package-initialize)

(unless (package-installed-p 'buttercup)
   (package-refresh-contents)
   (package-install 'buttercup))
